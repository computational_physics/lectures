{% extends 'markdown.tpl' %}

{% block input %}
{% if cell.metadata.attributes and cell.metadata.attributes.initialize %}
<div class="thebelab-init-code">
  <pre class="thebelab-code" data-executable="true" data-language="python">
    {{cell.source}}
  </pre>
</div>
{% endif %}
{% endblock %}

{% block display_data %}
{% if cell.metadata.attributes and cell.metadata.attributes.editable == 'false' and not cell.metadata.attributes.initialize %}
{{ super() }}
{% elif cell.metadata.attributes and cell.metadata.attributes.initialize %}
{% if output.data['text/html'] %}
<div>
  {{ output.data['text/html'] }}
</div>
{% endif %}
{% else %}
<pre class="thebelab-code" data-executable="true" data-language="python">{{cell.source}}</pre>
<div class="thebelab-output" data-output="true" markdown="1">
{% if output.data['text/html'] %}
{{ output.data['text/html'] }}
{% else %}
{{ super() }}
{% endif %}
</div>
{% endif %}
{% endblock %}

{% block error %}
{% endblock error %}

{% block stream %}
{%- if output.name == 'stdout' -%}
{{ output.text | indent }}
{%- endif -%}
{% endblock stream %}

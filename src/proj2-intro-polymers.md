# Monte Carlo simulations of polymers

This project is a bit different from the other two on the technical side:
instead of using the Metropolis algorithm (i.e. generating a Markov chain)
to sample the Boltzmann distribution, we use [approximate importance sampling](proj2-monte-carlo.md#approximate-importance-sampling)

The goal is to simulate the properties of long polymers in a good
solvent (the latter just means that the polymers do not want to stick
to other polymers in the solution, and thus we can simulate individual
polymers), and to study how they curl up.

To do this, we use a simple model for the polymer in 2D

- a polymer is a chain of subunits (monomers)
- the distance between monomers is fixed (1 in dimensionless units)
- the angle between neighboring bonds is fixed to multiples of $90^\circ$. With this, the subunits of the polymer are restricted to the sites of a square lattice.
- One lattice site can only be occupied by one monomer.

This set of rules turns the polymer problem into a *self-avoiding
random walk* on a square lattice.

![](/figures/polymer-selfavoiding.svg)

It would be possible to sample all self-avoiding random walks by starting
from a free random walk, and throwing away all paths with intersections.
However, this quickly becomes too expensive computationally!

In this project we instead employ a Monte Carlo method, to sample
only a finite number of representive polymers. In particular, we
employ the Rosenbluth that builds a polymer step by step: an existing polymer is grown
by adding a new subunit onto an unoccupied lattice site. In this way we sample
only allowed polymers. However, not every polymer then has the same
probability (as desired by us), as the growing process favors certain
configurations more than others!

In accordance with what we have learned for [approximate importance sampling](proj2-monte-carlo.md#importance-sampling),
we need to correct for this by introducing a weight. 
This is derived in detail in the [lecture notes for the polymer project](proj2-polymers.md)
In particular, make sure you understand how Eq. (2) is derived there :
$$\langle r^2(L) \rangle = \frac{ \sum_{k=1}^N w_k^{(L)} r^2_{k}(L)}{\sum_{k=1}^N w_k^{(L)}} \tag{2}$$

Your first task in this project is thus to implement the Rosenbluth algorithm.
To quantitatively investigate the curling of polymers, it is useful to study
the squared end-to-end distance or the squared radius of gyration of the polymers. Both
quantities are known to scale as $~L^{2\nu}$ with $\nu=3/4$. Reproduce this scaling behavior!
This can be compared to the case of a free random walk, where we know $\nu=1/2$.

After implementing the Rosenbluth algorithm, you will find that for
long polymers very few samples have a large weight and dominate,
giving a big error on the result. You can fix this problem by using a
more advanced sampling method, called PERM. Implement this as the
final step and show that PERM is indeed superior to Rosenbluth for
long polymers.

This project is again rather predefined, and what is described above is
the natural route for the project. If you have an additional idea,
feel free to discuss with us!

## Milestones

In the second project, we only provide you high-level milestones for every
week! You will need to define every week your own detailed milestones

- **Week 1**: Start to implement a basic simulation of polymers using the Rosenbluth algorithm, explain the weights entering the simulation, and make plans how to validate your code.
- **Week 2**: Finish the basic simulation and validate your results
- **Week 3**: Generate results (including error bars) and start to implement the PERM
algorithm. 


## Literature

For this project we have written [extended lecture notes with all relevant information](proj2-polymers.md)

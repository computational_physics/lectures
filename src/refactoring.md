# How to organize code: A refactoring example

## Lecture video

<iframe width=100% height=420 src="https://www.youtube-nocookie.com/embed/EQIOyF2oots?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Exercise material

[Notebook used in the video](https://gitlab.kwant-project.org/anton-akhmerov/refactoring)
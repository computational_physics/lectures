# How to plan a group programming project

In Computational Physics you will be working as a group on a project. 
One reason for this is simple - the projects are a lot of work for a single person.
In addition, working as a group on a project is industry-standard, particularly for programming projects.
Working in a group is thus also one of our learning goals.

However, to work efficiently as a team, you have to do some planning.
To this end we want to give you guidelines and tools. 

## Before you begin

Before starting to work, it is useful to align within the group:

- share your expectations and what you think about the task. Consider aspects such as your ambitions or experience levels.
- share any limitations you have, such as times when you cannot work
- lay down ground rules. Examples include statements such as "we treat each other with respect", but also agreements on 
  how decisions are made, what happens if someone misses a deadline, etc. 

The advantage of making these agreements before the group starts is that it is easier to make them then, rather then when something already happened!

## Planning tasks

Before working on a problem, make sure to include a proper planning for a sepcific timespan - for the course the timespan of a week seems appropriate.

1. Start by defining the goals/milestones that you would like to reach. A goal is a higher-level objective such as
   ```
   Compute the force between two atoms due to the Lennard-Jones potential
   ```
   *Note that for the first project many of the milestones of the first weeks are predefined.
   You will need to define your own milestones towards the end of project 1 as well as for the final two projects.*
2. Break the milestones down to ***specific*** tasks. Be as specific as possible, for example
   ```
   Write a function that takes positions of particles as input and
   returns an array of forces corresponding to each particle
   ```
3. Decide for each task whether to do it together as a group, or assign it to specific person(s).
   Clearly this will depend on the nature of the task. Tasks that affect the strategy of the project or decisions that have a
   lot of effect on later code (such as which data format to use) are best done together as a group. On the other hand, 
   tasks that affect coding of isolated components can easily be assigned to a specific person. Many tasks will be 
   in between those extremes, and you will need to try what works best for your group.
4. Write down how you assigned the task (group or specific person(s)), and set a fixed deadline, e.g.
   ```
   Write a function that takes positions of particles as input and
   returns an array of forces corresponding to each particle
   (Alice, by 24 Feb 2023)
   ```

## Engaging the whole group in coding

Distributing programming tasks has the danger that not the whole group is familiar
with the code. Especially as the code grows this will limit efficiency. Make sure
everyone is involved in coding! You can organize this in different ways

- Pair programming. Code pairs, where one person codes and the other asks questions or gives comments.
- Code review. Assign a person to review code that was written by someone else and set a deadline. Example:
   ```
   Write a function that takes positions of particles as input and
   returns an array of forces corresponding to each particle
   (Alice, by 24 Feb 2023; review by Bob until 25 Feb 2023)
   ```
- Do code review as a group. Also set a date for this!

## Preventing and managing conflicts.

Group work usually is rewarding. Still, sometimes conflicts arise. While we don't expect
you to get into a conflict, we would like to give you tools that can help to prevent
or manage conflicts. 

The key for this is *feedback*:

- Give constructive feedback to your group members. Often, they may not be aware of what bothers you
- Create an atmosphere where feedback is valued, and be open to feedback
- If things are going great, don't forget to give positive feedback!
- Make giving feedback a regular habit.

Feedback can help in "normal" conflict situations. *In the case of an unacceptable event,
directly contact an instructor or a student conselor.*

### Team checklist

Even with the best intentions, giving feedback, especially if it is negative, is hard.
It can help to formalize the process:

Answer the following questions about the functioning about the group
first looking at yourself (column "me"), and then looking at the
other group members (column "group")

|  | me | group |
|---|---|---|
| Are tasks done in time and meetings attended? |  |  | 
| Are all group members involved in decisions? |  |  | 
| Are everybody's ideas taken into account? |  |  | 
| Do group members contribute equally? |  |  | 
| Are the group members treated respectfully? |   |   |

Then use this form to reflect on the group work, and discuss how to improve for
the future. In this context, you will most likely need to bring up more specific
feedback. Again, it helps to follow a formalized process:

1. Give *factual observations*, i.e. don't say "XX is lazy", but rather "I have written 
the code for 5 out of 6 functions this week". By sticking to a factual observation you
create a common ground for all participants.
2. You can then state how this makes *you* feel.

In this way the feedback is based on realities (factual observations and your feelings)
and it is easier to have a discussion on it.


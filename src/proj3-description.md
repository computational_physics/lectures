# Project 3 - Free project

## Introduction

For the final project, you have a lot of freedom: we encourage you to define your own project.

The only requirement we ask for is that it should be a simulation of a
physics-related system and that the problem can be analyzed in a scientific way
(i.e. a systematic analysis instead of say only showing an animation).
Note that we consider physics-related rather broad, and e.g.
simulation of biological systems or an engineering problem are also absolutely
ok.

We do advise you though to start from a project where there is some existing literature.
in this way you have a starting point as well as something to compare your simulation to
for validation.

## Instructions

### What we expect you to do

Again, work in groups of two or three students. You may
work with the same group as in the other projects or choose new partners.

Come up with your own project, and discuss it with the course team. 
We strongly recommend to base yor project on existing literature. The course
team will assess whether your project is feasible within the 4 weeks of the
final project, and help you define the project.

After your project is approved by the course team, you will work on your own individual project!

### Milestones along the path

As in the other projects, we expect you to document your progress using milestones. 
Since this is your own project, you will define the milestones yourself.

## Resources

- Use any useful resource you can find, but remember that you need to disclose what you used.
  Also, the simulation code must be written by you. If in doubt, consult with the course team.

## Products

- Simulation code in a gitlab repository
- Final presentation
- Filled out weekly progress issues. These will be opened automatically in your
  gitlab repository, and need to be filled in before the next class.

## Assessment criteria

See the [details of the grading scheme here](proj3-grading.md)

## Supervision and help

* The lecturers and the TAs are present during the lectures and are
  willing to help with all your problems
* Out of class you are encouraged to ask any question on the course
  chat (preferred) or write an email to the lecturers.




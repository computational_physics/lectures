# Starting project 2

- Projects in this course are done in groups of 2 (or in exceptional cases 3) students.
  You may continue in the same group as for project 1.
- You should again register project 2 with us. Depending on which of the three
  possible projects you choose, go to the specified link and follow
  the instructions:
    - For "Monte Carlo simulations for the Ising model" go to [this link](https://gitlab.kwant-project.org/computational_physics/organization/issues/new?issuable_template=create_project_ising).
    - For "Variational Quantum Monte Carlo" go to [this link](https://gitlab.kwant-project.org/computational_physics/organization/issues/new?issuable_template=create_project_qmc).
    - For "Monte Carlo simulations of polymers" go to [this link](https://gitlab.kwant-project.org/computational_physics/organization/issues/new?issuable_template=create_project_polymer).
  An automated bot will open your repository soon after.
- In particular, be aware that a part of your grade is based on tracking the progress
  in the project (consult the file `journal.md` in your git repository for details).
  In this second project, you will define the detailed milestones yourself.

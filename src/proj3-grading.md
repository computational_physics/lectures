# Grading scheme

For the final project, you will receive a grade for the repository (keeping track of your project
development), and the final presentation.

## Repository (30% of project grade):

- (20% of project grade) Weekly issues report planning, project progress with respect to the weekly
  milestones, backed up with data, code and plots.<br>
  For every week, you can get 2 (:smile:), 1 (:expressionless:), or 0 (:disappointed:)
  points, awarded by the respective emoji to the respective review issue.<br>
  If you fail to meet all milestones within a week, the grading is postponed by one week and the grade capped at 1 point.
  If you still fail to meet all milestones by then, you will get 0 points.
- (10% of project grade) Every project member makes regular commits<br>
  "Regular" means that subsequent commits by one person shold not be more than two weeks
  apart.<br>
  Make sure every group member makes regular commits, this is graded by member, not
  project. Also, if a group member has no commits at all, they will not receive a grade
  for this project.


## Presentation (70% of the project grade):

- Explains the theoretical background and the motivation for the research question
- Documents correctness checks (for the final projects, there may not be a big distinction between validity check and final result, if you are reproducing the result of a paper)
- Data is reported with uncertainty, and fitted to the expected behavior if applicable
- A variety of phenomena are investigated scientifically (do not just show pictures or movies, make a proper analysis), and compared with known results.
- Performance is reported and discussed briefly
- The presentation slides are clear and illustrate the results/message well.
- The presentation is held in an engaging manner and within the alotted time. All group members contribute significantly to presenting.

Contrary to the report, we will not give separate grades for all of these aspects, but a single grade taking into account all of them

## Code (0% of the project grade)

The simulation code must be present, so that we can assess that you wrote your own simulation. A project without code is not graded! Otherwise, the code does not enter the final grade.
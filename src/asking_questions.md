# Asking efficient questions on a complex topic

## Why there is such a thing as an *inefficient* question

In any context where you need to learn something, you inevitably ask questions.
This is certainly true for a course, but also in professional contexts. 

Often in these contexts, questions are about rather complex topics and difficult
to answer. This means that the expert you are asking also needs to spend time to
give an answer. But time is a scarce resource, and you will maximize the chance for
getting a meaningful reply if you ask your question such that all the necessary
information is summarized and the question posed in a specific way - minimizing the
time necessary to answer it. Additionally, you show that you spent time yourself
thinking about the question which also helps to motivate people to look and think
about your question.

For this reasons, programming-help websites like stackoverflow have [detailed
instructions for asking questions](https://stackoverflow.com/help/how-to-ask).
We believe that these principles also hold in general when asking questions
about a complex problem. 

## Example of an inefficient questions

An extreme (but not uncommon) type of question we get in Computational
Physcs is of the form

![Example of inefficient question](figures/inefficient_question.png)

Let's analyze a bit more why this question is inefficient:

- First, it is important to understand that even experienced programmers
  who have studied many similar simulation codes **are in general not able
  to tell what goes wrong only by *looking* at code**. This is not how it
  works.
- The question is unspecific. What does "does not work" mean? Does the code
  raise a Python error? Is the shape of the object that is returned
  different from expectations? Does it work formally, but gives unphysical
  results?
- Context is missing. What should the code achieve? What are all those
  variables that are used? Is this an isolated function or is there more
  code around it?
- Code is given as a screenshot. So someone who would like to run some
  part of the code (which would be difficult here, as the context is missing)
  needs to type it in, rather than copy-paste.

Hence, anyone who would like to help with this question, has to ask all these
questions, before they even have a *chance* to answer it. This is inefficient.

## Guidelines for asking efficient questions

#### Do your own research beforehand

Spend time trying to figure out the problem yourself! Search if someone has
a similar problem in the forum. Try to google the answer.

#### Describe the problem clearly

- Be specific. What is the problem you are facing?
- Give context. What do you want to achieve?
- In the case of a code problem: Explain what needs to be done to observe a
  problem, e.g. what steps need to be done to see the problem? What kind of
  parameters are fed to functions?
- Explain what you expect to see or what you want to achieve.
- Describe what you actually see or what the current situation is.

#### Share supporting information

- For a simulation problem, add plots that show the simulation results,
  and compare to what you expect to see
- If a piece of code is rather self-contained and not too long (<10-20 lines),
  add it to the question. Make sure the code is copy-pasteable (in our forum,
  use the "preformatted text" tags).
- If the corresponding piece of code is longer, don't copy it in the question.
  Better add a link to the code as stored on e.g. gitlab. For example,
  for a Python file you can get a link to a specific line of code by opening
  the file in the gitlab web interface, and clicking on the line number.

#### Describe your thoughts about the problem

- Describe what you already tried to solve the problem, but didn't help
- Give your suspicions/thoughts about the problem.


#### Before posting the question

- Proof-read your question with respect to these guidelines
- Make sure the question has a problem-specific title

## Examples of more efficient questions

How could this look in practice? Let's look at some examples.

#### Example 1: How can I vectorize the `for`-loop in the Euler method?

We have implemented the Euler method to solve the time-dynamics
governed by Newton's equation. Currently, we are doing this
using a `for`-loop, but we would like to use `numpy` instead.

Currently the code for one time-step looks as follows:

``` py
for i in range(Nparticles):
    for d in range(dim):
    	pos[i, d] = oldpos[i, d] + vel[i, d] * delta_t
	vel[i, d] = oldvel[i, d] + force[i, d] * delta_t
```

The position and velocity of the previous step are stored in `oldpos` and
`oldvel`, and the current force in `force`. All these arrays are
of size `Nparticles` x `dim`, where `Nparticles` is the number of particles and
`dim=3`.

We were able to remove one for loop by writing this as

``` py
for i in range(Nparticles):
    pos[i, :] = oldpos[i, :] + vel[i, :] * delta_t
    vel[i, :] = oldvel[i, :] + force[i, :] * delta_t
```
but how can we get rid of the outer `for`-loop? Or is this impossible?

#### Example 2: Unexpected simulated orbit in planetary system

We are trying to simulate the movement of the earth orbiting the sun.

To this end, we use the Euler method to numerically integrate Newton's equation
for two bodies interacting with the graviational force. We use the masses
of the earth and the sun.

We know that the resulting orbit should be an ellipse/circle, but when we
run the simulation we always find that the earth is spiralling into the sun:

![](figures/spiralling_earth.png)

The notebook generating this plot can be found at [this link](https://compphys.quantumtinkerer.tudelft.nl/downloads/proj1-moldyn-week3-euler-vs-verlet.ipynb).

We have already tried to see if this behavior depends on the step size. If we
set the step size smaller, the spiralling behavior gets less, but we still
don't find closed trajectories as we expect.

We want to do a long-term simulation, so this behavior is a deal-breaker for
us. Do we have to use a higher-order integration method? 


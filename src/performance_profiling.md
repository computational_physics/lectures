# Performance I - Profiling

## Lecture video
<iframe width=100% height=540 src="https://www.youtube-nocookie.com/embed/FVkMFas_bsg?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Exercise material

[Notebook and code used in the video](https://gitlab.kwant-project.org/michaelwimmer/performance_and_profiling/-/tree/master/1_profiling)
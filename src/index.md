# Computational Physics

!!! summary "Learning goals"

    After following this course you will be able to:

    - describe standard physical simulation algorithms.
    - implement these algorithms in a well-structured computer code.
    - apply these algorithms to calculate physical properties and interpret the outcome.
    - verify correctness of the simulation outcome and use this analysis to improve the code.
    - organize and perform joint programming work.
    - present the results according to the requirements of scientific communication.     - 

#### Notes on the lecture notes

In these notes our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/computational_physics/lectures), especially do [let us know](https://gitlab.kwant-project.org/computational_physics/lectures/issues/new?issuable_template=typo) if you see a typo!

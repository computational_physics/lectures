# Variational Quantum Monte Carlo

The stationary Schrödinger equation can only be solved exactly for
some specific examples. In general, and certaily for interacting
systems, an exact solution is impossible. Still, we can try to find
the approximate ground state energy using a variational calculation.

To this end, we consider a *trial wave function* $\psi_T(\mathbf{r},
\alpha)$, where $\alpha$ is one or more variational parameters
and $\mathbf{r}$ denotes the spatial degrees of freedom. We
can compute the expectation value of the energy as

$$
E(\alpha) = \frac{\int d\mathbf{r}\,
\psi_T^*(\mathbf{r},\alpha)\, H\, \psi_T(\mathbf{r}, \alpha)}{\int
d\mathbf{r}\, \psi_T^*(\mathbf{r},\alpha) \psi_T(\mathbf{r}, \alpha)}
$$

The variational principle tells that $E(\alpha) \geq$ the real
ground state energy. The best approximation to the ground state energy
is thus obtained by minimizing $E(\alpha)$ with respect to
$\alpha$. Obviously, the quality of the approximation depends on how
good the trial wave function is.

The Monte Carlo method is in this case used to compute the integral
over the spatial degrees of freedom of the wave function $\mathbf{r}$.
For the case of the helium atom, these are 6 degrees of freedom
(positions of 2 electrons in 3D space), and this is high-dimensional enough
for the Monte-Carlo method to be useful.

To get a proper probability, we define the local energy as

$$
E_\mathrm{loc}(\mathbf{r}, \alpha) = \frac{H
\psi_T(\mathbf{r}, \alpha)}{\psi_T(\mathbf{r}, \alpha)}
$$

Then the energy can be computed as

$$
E(\alpha) = \frac{\int d\mathbf{r}\,
\psi_T^*(\mathbf{r},\alpha)\psi_T(\mathbf{r}, \alpha)\,
E_\mathrm{loc}(\mathbf{r}, \alpha)}{\int d\mathbf{r}\,
\psi_T^*(\mathbf{r},\alpha) \psi_T(\mathbf{r}, \alpha)}
$$

and we can solve this integral using Monte Carlo integration with
importance sampling according to the probability density $\psi_T^*
\psi_T/\int d\mathbf{r} \psi_T^* \psi_T$ - which we can again sample
exactly using the [Metropolis algorithm](proj2-monte-carlo.md#metropolis-monte-carlo).
The configuration space we sample then is given by the position
$\mathbf{r}$ (or the positions $\mathbf{r}_1$ and $\mathbf{r}_2$
for the Helium atom). A Monte Carlo
move then consists of chosing a new position $\mathbf{r}'$ randomly
from a region around the old position. Make sure you do this such that
the probability of going forth and back are the same, as required by
detailed balance (this is achieved most easily by using Cartesian
coordinates, and chosing rectangular regions). Also, make sure to
chose a region of a size such that the acceptance ratio is not too big
or too small.

As a start, apply the variational Monte Carlo method on a solvable
model, such as a harmonic oscillator or the hydrogen atom. You will need to

- derive the expression for the local energy for a given trial wave function
- write a code that can implements the Monte carlo integration. Make sure
  you make a move such that the overall acceptance ratio is not too low or too
  high.
- minimize the energy with respect to $\alpha$. This is a bit more subtle as it seems, as the Monte Carlo result for $E(\alpha)$ has an error associated with it, and the usual minimization algorithms may be thrown off track. Care must be thus taken to compute the gradient.

After that, solve the problem for the helium atom for which no exact
analytical solution is given.

This project is thus rather predefined and follows a fixed path. There
is less freedom as other applications than mentioned above are more
difficult to implement. Yet, if you come up with an idea, feel free
to discuss with us!

## Milestones

In the second project, we only provide you high-level milestones for every
week! You will need to define every week your own detailed milestones.

- **Week 1**: Start to iplement a basic simulation to compute the expectation value of the energy for a given set of variational parameters, using the Metropolis algorithm. Argue why your approach fulfils detailed balance and make plans how to validate your code.
- **Week 2**: Finish the basic simulation, monitor the acceptance ratio and validate your results.
- **Week 3**: Generate results for the Helium atom (including error bars) and start to implement the proper minimization of the energy.

## Literature

- details on the variational method can be found in [Chapter 4 of the lecture notes of Applications of Quantum Mechanics](/downloads/aqm.pdf)
- implementation details of the Monte Carlo method in [Chapter 12.2](/downloads/Thijssen_Chapter12_Quantum_Monte_Carlo.pdf)  from the book "Computational Physics" by Jos Thijssen.


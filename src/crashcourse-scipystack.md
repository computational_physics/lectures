# Introduction to the Scipy stack

## Lecture video

<iframe width=100% height=420 src="https://www.youtube-nocookie.com/embed/c9X3iTd-BUE?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Exercise material

[Notebooks introducing the scipy stack, with exercises and solutions](https://gitlab.kwant-project.org/computational_physics/computing_crash_course/-/tree/master/numpy_scipy)

(part of the [computing crash course material](https://gitlab.kwant-project.org/computational_physics/computing_crash_course))

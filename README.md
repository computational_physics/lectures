# compphys

Lecture notes and teaching material used for the Delft University of Technology course awesome course.

The compiled materials are available at https://compphys.quantumtinkerer.tudelft.nl

# Technical instructions

## Writing the lecture notes

When you add new content to the lecture notes, work in a separate git branch. Only
merge when you are satisfied with the results.

The website is written using markdown. All features as known from the jupyter notebook are
supported (additional extensions can be specified in`mkdocs.yml`). All `.md` files are to
be placed in the folder `src`. For them to show up in the index, they have to be added to 
`mkdocs.yml` under the `nav:` entry in the form
```
  - Name in index: 'filename.md'
```
Entries in the index can also be nested:
```
  - Name of entry:
    - Name of first subentry: 'filename1.md'
    - Name of second subentry: 'filename2.md'
```

It is also possible to have the output of python code to be incorporated in the lecture notes.
To this end, add fenced code blocks like this
````
```{.python .input}
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 2*np.pi)
y = np.sin(x)
plt.plot(x, y)
plt.show()
```
````
Internally, those code blocks are converted to be the cells of a jupyter notebook. Like notebook cells,
variables are valid across different code blocks. If you have code that you would like to import
from a separate `.py`-file, put that file into `code`. It will then be visible directly for `import`.

## Deploying changes to the webserver

Changes you make to the documentation are directly deployed to the webserver after pushing.

If you work in a separate branch called `branch_name`, the changes are deployed to
`https://computationalphysics.quantumtinkerer.tudelft.nl/test_builds/branch_name`. In this way you can
check intermediate stages without exposing this to the students directly.

## Testing the build locally

The website can also be built locally by calling
```
python execute.py; mkdocs serve
```
Make sure the packages
```
mkdocs mkdocs-material python-markdown-math notedown
```
are installed.

# Origin and technical support

This repository is based on a template for publishing lecture notes, developed
by Anton Akhmerov, who also hosts such repositories for other courses.
